<?php

class Model_Building
{
    /**
     * passengers qty to be created for script
     */
    const PASSENGERS_QTY = 8;

    /**
     * floor qty to be created for script
     */
    const FLOOR_QTY = 5;

    public $elevator;

    public $passengers = array();

    public $passengersByFloor = array();

    public function __construct()
    {
        $this->_initElevator();
        $this->_createPassengers();

    }

    protected function _initElevator()
    {
        $this->elevator = new Model_Elevator($this);
        echo '<p>Elevator for ' . Model_Elevator::MAX_PASSENGER_QTY_IN_ELEVATOR . ' persons is on ' . $this->elevator->currentFloor . ' floor</p>';
    }

    protected function _createPassengers()
    {
        for ($i = 1; $i <= self::PASSENGERS_QTY; $i++) {
            $pass = new Model_Passenger($this->elevator, $i);
            $this->passengersByFloor[$pass->currentFloor][] = $pass;
            $this->passengers[] = $pass;
        }

        echo '<p>Passengers are: </p>';
        ksort($this->passengersByFloor);
        foreach ($this->passengersByFloor as $floor => $passangers) {
            echo '<p>' . count($passangers) . ' passenger(s) on ' . $floor . ' floor</p>';
            echo '<ul>';
            foreach ($passangers as $pass) {
                echo '<li>' . $pass->name . ' to ' . $pass->destinationFloor . ' floor</li>';
            }
            echo '</ul>';

        }
        echo '#############################';
    }

    public function start()
    {
        foreach ($this->passengers as $pass) {
            $pass->callElevator();
        }
        $this->elevator->move();
    }
}
