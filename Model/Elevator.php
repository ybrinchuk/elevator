<?php

class Model_Elevator
{
    const MAX_PASSENGER_QTY_IN_ELEVATOR = 4;

    const DIRECTION_UP = 'up';

    const DIRECTION_DOWN = 'down';

    public $passengersInElevator = array();

    public $currentFloor;

    public $direction = null;

    public $innerButtons = array();

    public $outerButtons = array();

    protected $_inProgress = false;

    protected $_doorIsOpened = false;

    protected $_targetFloor = null;

    protected $_building;


    public function __construct(Model_Building $building)
    {
        $this->_building = $building;
        $this->currentFloor = rand(1, Model_Building::FLOOR_QTY);
        $this->currentFloor = 1;
    }

    public function move()
    {
        if ($this->direction == self::DIRECTION_UP) {
            $this->_moveUp();
        }
        if ($this->direction == self::DIRECTION_DOWN) {
            $this->_moveDown();
        }
    }

    protected function _moveUp()
    {
        while ($this->_targetFloor !== null && $this->direction == self::DIRECTION_UP) {
            $this->currentFloor++;
            echo '<p>' . $this->currentFloor . ' floor</p>';
            if (isset($this->innerButtons[$this->currentFloor]) || $this->currentFloor == $this->_targetFloor) {
                $this->_arrived();
            }
        }
        $this->move();
    }

    protected function _moveDown()
    {
        while ($this->_targetFloor !== null && $this->direction == self::DIRECTION_DOWN) {
            $this->currentFloor--;
            echo '<p>' . $this->currentFloor . ' floor</p>';
            if (isset($this->innerButtons[$this->currentFloor]) || isset($this->outerButtons[$this->currentFloor])
                || $this->currentFloor == $this->_targetFloor
            ) {
                $this->_arrived();
            }
        }
        $this->move();
    }


    protected function _arrived()
    {
        unset($this->innerButtons[$this->currentFloor]);
        unset($this->outerButtons[$this->currentFloor]);

        if ($this->currentFloor == $this->_targetFloor) {
            $this->_targetFloor = null;
            $this->direction = null;
        }

        $this->_openDoor();

        foreach ($this->passengersInElevator as $key => $passenger) {
            if ($this->currentFloor == $passenger->destinationFloor) {
                $passenger->goOut();
                unset($this->passengersInElevator[$key]);
            }
        }

        foreach ($this->_building->passengersByFloor[$this->currentFloor] as $key => $newPassenger) {
            try {
                $newPassenger->comeIn();
                $this->passengersInElevator[] = $newPassenger;
                unset($this->_building->passengersByFloor[$this->currentFloor][$key]);
                if (empty($this->_building->passengersByFloor[$this->currentFloor])) {
                    unset($this->_building->passengersByFloor[$this->currentFloor]);
                }
            } catch (Exception $e) {
                echo '<p>' . $e->getMessage() . '</p>';
            }
        }
        $this->_closeDoor();
        $this->_getLostPassengers();
        $this->_resetTarget();
    }

    protected function _getLostPassengers()
    {
        // if we left someone because there is no place or wrong direction
        if (!empty($this->_building->passengersByFloor[$this->currentFloor])) {
            foreach ($this->_building->passengersByFloor[$this->currentFloor] as $pass) {
                $pass->callElevator();
            }
        }
    }

    protected function _resetTarget()
    {
        if ($this->direction == self::DIRECTION_UP) {
            krsort($this->innerButtons);
            reset($this->innerButtons);
            $key = key($this->innerButtons);
            if ($key > $this->_targetFloor) {
                $this->_targetFloor = $key;
            }
        }

        if ($this->direction == self::DIRECTION_DOWN) {
            ksort($this->innerButtons);
            reset($this->innerButtons);
            $key = key($this->innerButtons);
            if ($key < $this->_targetFloor) {
                $this->_targetFloor = $key;
            }
        }

        if ($this->_targetFloor === null && empty($this->passengersInElevator) && !empty($this->outerButtons)) {
            if (isset($this->outerButtons[1])) {
                $this->_targetFloor = 1;
            } else {
                krsort($this->outerButtons);
                reset($this->outerButtons);
                $key = key($this->outerButtons);
                $this->_targetFloor = $key;
            }
            $this->setDirection();
        }

        if ($this->_targetFloor === null && empty($this->innerButtons) && empty($this->outerButtons)) {
            $this->_inProgress = false;
            exit('FINISH');
        }


    }

    public function setInnerButton($floor)
    {
        //passenger come in empty elevator can change direction
        if (empty($this->passengersInElevator)) {
            $this->_targetFloor = $floor;
            $this->setDirection();
        }

        $this->innerButtons[$floor] = true;

    }

    public function setOuterButton($floor)
    {
        $this->outerButtons[$floor] = true;

        // if the elavator called the first time it starts to move to this passenger immediately
        if (!$this->_inProgress) {
            $this->_inProgress = true;
            $this->_targetFloor = $floor;
            $this->setDirection();
            if ($this->currentFloor != $floor) {
                echo '<p> Elevator starts to move to ' . $this->_targetFloor . ' floor</p>';
            } else {
                $this->_arrived();
            }
        }
    }

    protected function setDirection()
    {
        if ($this->currentFloor == 1) {
            $this->direction = self::DIRECTION_UP;
        } elseif ($this->currentFloor == Model_Building::FLOOR_QTY) {
            $this->direction = self::DIRECTION_DOWN;
        } elseif ($this->_targetFloor > $this->currentFloor) {
            $this->direction = self::DIRECTION_UP;
        } elseif ($this->_targetFloor < $this->currentFloor) {
            $this->direction = self::DIRECTION_DOWN;
        }
    }


    protected function _closeDoor()
    {
        if ($this->_doorIsOpened) {
            $this->_doorIsOpened = false;
            echo '<p>Close the door</p>';
        }
    }

    protected function _openDoor()
    {
        if (!$this->_doorIsOpened) {
            $this->_doorIsOpened = true;
            echo '<p>Open the door</p>';
        }
    }

}
