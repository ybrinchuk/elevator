<?php

class Model_Passenger
{

    public $currentFloor;

    public $destinationFloor;

    public $name;

    public $direction;

    protected $_elevator;


    public function __construct(Model_Elevator $elevator, $count)
    {
        $this->_elevator = $elevator;

        $this->name = 'passenger' . $count;

        $this->currentFloor = rand(1, Model_Building::FLOOR_QTY);

        do {
            $this->destinationFloor = rand(1, Model_Building::FLOOR_QTY);
        } while ($this->currentFloor == $this->destinationFloor);

        if ($this->currentFloor > $this->destinationFloor) {
            $this->direction = Model_Elevator::DIRECTION_DOWN;
        } else {
            $this->direction = Model_Elevator::DIRECTION_UP;
        }
    }


    public function callElevator()
    {
        if (!isset($this->_elevator->outerButtons[$this->currentFloor])) {
            echo '<p>Elevator is called from ' . $this->currentFloor . ' floor by ' . $this->name . '</p>';
            $this->_pressOuterButton();
        }
    }

    public function goOut()
    {
        echo '<p>' . $this->name . ' out from elevator </p>';
    }


    public function comeIn()
    {
        if (count($this->_elevator->passengersInElevator) >= Model_Elevator::MAX_PASSENGER_QTY_IN_ELEVATOR) {
            throw new Exception($this->name . ' - The Elevator is full.');
        }

        if (($this->direction != $this->_elevator->direction) && !empty($this->_elevator->passengersInElevator)) {
            throw new Exception($this->name . ' - Not match direction.');
        }

        echo '<p>' . $this->name . ' come in elevator </p>';
        if (!isset($this->_elevator->innerButtons[$this->destinationFloor])) {
            echo '<p>' . $this->name . ' press button ' . $this->destinationFloor;
            $this->_pressInnerButton();
        }

        return true;
    }


    protected function _pressInnerButton()
    {
        $this->_elevator->setInnerButton($this->destinationFloor);
    }

    protected function _pressOuterButton()
    {
        $this->_elevator->setOuterButton($this->currentFloor);
    }
}
